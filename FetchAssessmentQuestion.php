<?php
if($_SERVER['REQUEST_METHOD']=="GET"){
    include 'Config.php';
    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata);
    try{
        $sql = "SELECT * FROM assessment_questions,assessment_answers 
                WHERE assessment_questions.assessment_question_id = assessment_answers.question_id 
                ORDER BY assessment_questions.assessment_question_id"; 
        $result = $conn->query($sql);
        if (!empty($result) && $result->num_rows > 0) {
            $i=0;
            $response = array();
            $assessment_choices = array();
            $resultData = $result->fetch_all(MYSQLI_ASSOC);
            foreach($resultData as $row ) {
                $assessment_choices[] = array(
                    "answer_key" => $row["answer_key"],
                    "answer" => $row["answer"],
                    "answer_tn" => $row["answer_tn"],
                    "question_id" => $row["question_id"],
                    "id" => $row["id"],
                    "is_correct" => $row["is_correct"]
                );
                if( !isset( $resultData[$i+1]) || ( $resultData[$i]["assessment_question_id"] != $resultData[$i+1]["assessment_question_id"])){
                    $response[] = array(
                        "assessment_question_id" => $row["assessment_question_id"],
                        "assessment_question" => $row["assessment_question"],
                        "assessment_question_tn" => $row["assessment_question_tn"],
                        "assessment_choices" => $assessment_choices
                    );
                    unset($assessment_choices);
                }  
                $i++;
            }
            $resultt=array("response"=>$response);
            echo json_encode($resultt);
        }
        else{
            echo $conn->$sql;
            require_once('InvalidCode.php');
        }
    }
    catch(conn_sql_exception $e){
        http_response_code(409);
        throw $e;
    }
    finally{
        $conn -> close();
    }
}
?>