<?php
if($_SERVER['REQUEST_METHOD']=="GET"){
    include 'Config.php';
 
    $id = $_GET["id"];
    $type =  $_GET["type"];
    try{
        $sql1 = "SELECT * from assessment_questions";
        $result1 = $conn->query($sql1);
        $sql2 = "SELECT * from assessment_score where  user_id =$id and assessment_type ='$type'";
        $result2 = $conn->query($sql2);
        $correctAnswer = $conn->query("SELECT * FROM assessment_score JOIN assessment_answers ON assessment_score.question_id = assessment_answers.question_id AND assessment_score.score = assessment_answers.id AND assessment_score.assessment_type ='$type' AND assessment_answers.is_correct =1 AND assessment_score.user_id = '$id'")->num_rows;

        if (!empty($result2) && $result2->num_rows > 0) {
            $i=1;
            $array = array();
            while($row = $result2->fetch_assoc()) {
                $array1 = array(
                    "user_id" => $row["user_id"], 
                    "question_id" => $row["question_id"], 
                    "assessment_type" => $row["assessment_type"], 
                    "score" => $row["score"]
                );
                $array[] = $array1;
            }
                $resultt=array("AnsweredAll"=>$result1->num_rows==$result2->num_rows,"AnsweredQuestions"=>$array,"CorrectAnswers"=>$correctAnswer);
                echo json_encode($resultt);
        }
        else{
            $resultt=array("AnsweredAll"=>false,"AnsweredQuestions"=>[],"CorrectAnswers"=>0);
        }
    }
    catch(conn_sql_exception $e){
        http_response_code(409);
        throw $e;
    }
    finally{
        $conn -> close();
    }
}
?>