<?php
if($_SERVER['REQUEST_METHOD']=="GET"){
    include 'Config.php';
    $chapterId = $_GET["chapter_id"];
    $user_id = $_GET["user_id"];
    try{
       $sql = "SELECT answers.id as answer_id, questions.id as question_id, questions.title as title, questions.title_tn as title_tn, answers.answer_key as answer_key, answers.answer_tn as answer_tn, answers.answer as answer, answers.is_correct as is_correct, chapters.id as chapter_id, chapters.name as chapter_name, chapters.video_url as video_url, chapters.facilitator_name as faiclitator FROM questions,answers,chapters 
                WHERE questions.id = answers.question_id  AND questions.chapter_id='$chapterId' AND chapters.id ='$chapterId'
                ORDER BY questions.id"; 
        $result = $conn->query($sql);
        $qtn = $conn->query("SELECT * from questions where chapter_id = '$chapterId'")->num_rows;
        $answer = $conn->query("SELECT * from chapter_score where user_id='$user_id' AND chapter_id = '$chapterId' ")->num_rows;
        $correctAnswer = $conn->query("SELECT * FROM chapter_score JOIN answers ON chapter_score.question_id = answers.question_id AND chapter_score.score = answers.id AND chapter_score.chapter_id ='$chapterId' AND answers.is_correct =1 AND chapter_score.user_id = '$user_id'")->num_rows;
        $passPercentage = ($correctAnswer/$qtn) * 100;
        if (!empty($result) && $result->num_rows > 0) {
            $i=0;
            $response = array();
            $choices = array();
            $chapter = array();
            $resultData = $result->fetch_all(MYSQLI_ASSOC);
            foreach($resultData as $row ) {
                $chapter = array(
                 "chapter_id" => $row["chapter_id"],
                 "chapter_name" => $row["chapter_name"],
                 "facilitator" => $row["faiclitator"],
                 "video_url" => $row["video_url"],
                 "correctAnswers" => $correctAnswer,
                 "isPassed" =>  ($qtn == $answer) && ($passPercentage > 74) ? true : false,
                 "isFailed" =>  ($qtn == $answer) && ($passPercentage < 75) ? true : false
                );
                $choices[] = array(
                    "answer_key" => $row["answer_key"],
                    "answer" => $row["answer"],
                    "answer_tn" => $row["answer_tn"],
                    "question_id" => $row["question_id"],
                    "is_correct" => $row["is_correct"],
                    "id" =>  $row["answer_id"],
                );
                if( !isset( $resultData[$i+1]) || ( $resultData[$i]["question_id"] != $resultData[$i+1]["question_id"])){
                    $response[] = array(
                        "question_id" => $row["question_id"],
                        "question" => $row["title"],
                        "question_tn" => $row["title_tn"],
                        "choices" => $choices
                    );
                    unset($choices);
                }  
                $i++;
            }
             $sql1 = "SELECT * from video where 
                (user_id=$user_id AND chapter_id=$chapterId)";
            $result1 = $conn->query($sql1);
            $data = $result1->fetch_assoc();
            $chapter["last_seen"] = $data && $data["last_duration"] ? $data["last_duration"] : 0;
            $resultt=array("response"=>$response, "chapter" => $chapter );
            echo json_encode($resultt);
        }
        else{
            echo $conn->$sql;
            require_once('InvalidCode.php');
        }
    }
    catch(conn_sql_exception $e){
        http_response_code(409);
        throw $e;
    }
    finally{
        $conn -> close();
    }
}
?>