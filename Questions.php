<?php
if($_SERVER['REQUEST_METHOD']=="GET"){
    include 'Config.php';
    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata);
    try{
        $sql = "SELECT * FROM questions,answers 
                WHERE questions.id = answers.question_id 
                ORDER BY questions.id"; 
        $result = $conn->query($sql);
        if (!empty($result) && $result->num_rows > 0) {
            $i=0;
            $response = array();
            $choices = array();
            $resultData = $result->fetch_all(MYSQLI_ASSOC);
            foreach($resultData as $row ) {
                $choices[] = array(
                    "id" => $row["id"],
                    "answer_key" => $row["answer_key"],
                    "answer" => $row["answer"],
                    "answer_tn" => $row["answer_tn"],
                    "question_id" => $row["question_id"],
                    "is_correct" => $row["is_correct"]
                );
                if( !isset( $resultData[$i+1]) || ( $resultData[$i]["id"] != $resultData[$i+1]["id"])){
                    $response[] = array(
                        "id" => $row["id"],
                        "course_id" => $row["course_id"],
                        "chapter_id" => $row["chapter_id"],
                        "title" => $row["title"],
                        "title_tn" => $row["title_tn"],
                        "choices" => $choices
                    );
                    unset($choices);
                }  
                $i++;
            }
            $resultt=array("response"=>$response);
            echo json_encode($resultt);
        }
        else{
            echo $conn->$sql;
            require_once('InvalidCode.php');
        }
    }
    catch(conn_sql_exception $e){
        http_response_code(409);
        throw $e;
    }
    finally{
        $conn -> close();
    }
}
?>