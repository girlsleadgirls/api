<?php
if($_SERVER['REQUEST_METHOD']=="GET"){
    include 'Config.php';
    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata);

    try{
        $sql = "SELECT * from chapters";
        $result = $conn->query($sql);
        if (!empty($result) && $result->num_rows > 0) {
            $list = array();
            while($row = $result->fetch_assoc()) {
                require_once('ValidCode.php');
                $res = $row["name"];
                $list[] = array("Chapter"=>$res);
            } 
            echo json_encode($list);
        }
        else{
            require_once('InvalidCode.php');
        }
    }
    catch(conn_sql_exception $e){
        http_response_code(409);
        throw $e;
    }
    finally{
        $conn -> close();
    }
}
?>