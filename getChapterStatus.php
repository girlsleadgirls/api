<?php
if($_SERVER['REQUEST_METHOD']=="GET"){
    include 'Config.php';
    $id = $_GET['id'];
    try{
        $assessmentQtn = $conn->query("SELECT * from assessment_questions")->num_rows;
        $preAssessmentAnswer = $conn->query("SELECT * from assessment_score where user_id='$id' AND assessment_type='pre'")->num_rows;
        $preAssessmentCorrectAnswer = $conn->query("SELECT * FROM assessment_score JOIN assessment_answers ON assessment_score.question_id = assessment_answers.question_id AND assessment_score.score = assessment_answers.id AND assessment_score.assessment_type ='pre' AND assessment_answers.is_correct =1 AND assessment_score.user_id = '$id'")->num_rows;
        $postAssessmentAnswer = $conn->query("SELECT * from assessment_score where user_id='$id' AND assessment_type='post'")->num_rows;
        $postAssessmentCorrectAnswer = $conn->query("SELECT * FROM assessment_score JOIN assessment_answers ON assessment_score.question_id = assessment_answers.question_id AND assessment_score.score = assessment_answers.id AND assessment_score.assessment_type ='post' AND assessment_answers.is_correct =1 AND assessment_score.user_id = '$id'")->num_rows;
        $i = 1;
        $status[]= array(
            "key"=>"preAssessment",
            "isDone" =>  $assessmentQtn == $preAssessmentAnswer ? true : false,
            "name" => "Pre Assessment",
            "answered_questions" => $preAssessmentAnswer,
            "total_questions" => $assessmentQtn,
            "correct_answers" => $preAssessmentCorrectAnswer,
            "sort_number" => $i
        );
        $i++;
        $result = $conn->query(" select * from chapters");
        while($row = $result->fetch_assoc()) {
            $chapterId = $row["id"];

            $qtn = $conn->query("SELECT * from questions where chapter_id = '$chapterId'")->num_rows;
            $answer = $conn->query("SELECT * from chapter_score where user_id='$id' AND chapter_id = '$chapterId' ")->num_rows;
            //echo "<br> '$answer'";
            $correctAnswer = $conn->query("SELECT * FROM chapter_score JOIN answers ON chapter_score.question_id = answers.question_id AND chapter_score.score = answers.id AND chapter_score.chapter_id ='$chapterId' AND answers.is_correct =1 AND chapter_score.user_id = '$id'")->num_rows;
            $passPercentage = ($correctAnswer/$qtn) * 100;
            $status[] =  array(
                "key" => $chapterId,
                "isDone" =>  $qtn == $answer ? true : false,
                "isPassed" => ($qtn == $answer) && ($passPercentage > 74) ? true : false,
                "name" => $row["name"],
                "answered_questions" => $answer,
                "total_questions" => $qtn,
                "correct_answers" => $correctAnswer,
                "sort_number" => $i
            );
            $i++;
        }
        $status[]= array(
            "key" => "postAssessment",
            "isDone" =>  $assessmentQtn == $postAssessmentAnswer ? true : false,
            "name" => "Post Assessment",
            "answered_questions" => $postAssessmentAnswer,
            "total_questions" => $assessmentQtn,
            "correct_answers" => $postAssessmentCorrectAnswer,
            "sort_number" => $i
        );
        echo json_encode($status);
    }
    catch(conn_sql_exception $e){
        http_response_code(409);
        throw $e;
    }
    finally{
        $conn -> close();
    }
}
?>