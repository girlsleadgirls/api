-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 08, 2020 at 01:08 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tycl_courses`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE `answers` (
  `id` int(11) NOT NULL,
  `answer_key` varchar(20) NOT NULL,
  `answer` text NOT NULL,
  `answer_tn` text NOT NULL,
  `question_id` int(11) NOT NULL,
  `is_correct` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`id`, `answer_key`, `answer`, `answer_tn`, `question_id`, `is_correct`) VALUES
(1, 'INDIA', 'India', 'இந்தியா', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `assessment_answers`
--

CREATE TABLE `assessment_answers` (
  `id` int(11) NOT NULL,
  `answer_key` varchar(20) NOT NULL,
  `answer` text NOT NULL,
  `answer_tn` text NOT NULL,
  `question_id` int(11) NOT NULL,
  `is_correct` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `assessment_answers`
--

INSERT INTO `assessment_answers` (`id`, `answer_key`, `answer`, `answer_tn`, `question_id`, `is_correct`) VALUES
(1, 'INDIA', 'India', 'இந்தியா', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `assessment_questions`
--

CREATE TABLE `assessment_questions` (
  `assessment_question_id` int(11) NOT NULL,
  `assessment_question` text NOT NULL,
  `assessment_question_tn` mediumtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assessment_questions`
--

INSERT INTO `assessment_questions` (`assessment_question_id`, `assessment_question`, `assessment_question_tn`) VALUES
(1, 'fefefWEFWEF', 'wfefwffwfefe'),
(2, 'fwefweafgstgegetg', 'hrthhrhsrth'),
(3, 'English', 'Tamil');

-- --------------------------------------------------------

--
-- Table structure for table `assessment_score`
--

CREATE TABLE `assessment_score` (
  `assessment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `assessment_type` varchar(11) NOT NULL,
  `score` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assessment_score`
--

INSERT INTO `assessment_score` (`assessment_id`, `user_id`, `question_id`, `assessment_type`, `score`) VALUES
(1, 1, 1, 'pre', 2),
(2, 1, 2, 'pre', 2),
(3, 1, 3, 'pre', 2),
(4, 1, 4, 'pre', 4),
(5, 2, 5, 'pre', 1);

-- --------------------------------------------------------

--
-- Table structure for table `chapters`
--

CREATE TABLE `chapters` (
  `id` int(22) NOT NULL,
  `course_id` int(22) NOT NULL,
  `name` varchar(100) NOT NULL,
  `video_url` text NOT NULL,
  `facilitator_name` varchar(100) NOT NULL,
  `video_duration` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chapters`
--

INSERT INTO `chapters` (`id`, `course_id`, `name`, `video_url`, `facilitator_name`, `video_duration`) VALUES
(1, 1, 'Physical Defense', '', '', ''),
(2, 1, 'Sexual Defense', '', '', ''),
(3, 1, 'Intellectual Defence', '', '', ''),
(4, 1, 'Online Defence', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `chapter_score`
--

CREATE TABLE `chapter_score` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `chapter_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `score` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chapter_score`
--

INSERT INTO `chapter_score` (`id`, `user_id`, `chapter_id`, `question_id`, `score`) VALUES
(1, 13, 2, 4, 9),
(2, 1, 2, 4, 4);

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(22) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `name`) VALUES
(1, 'girls lead girls');

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `module_id` int(11) NOT NULL,
  `module_name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `chapter_id` int(11) NOT NULL,
  `title` text NOT NULL,
  `title_tn` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `course_id`, `chapter_id`, `title`, `title_tn`) VALUES
(1, 1, 1, 'Who are you?', ''),
(2, 1, 4, 'What do you want?', ''),
(3, 1, 2, 'What\'s your name?', ''),
(4, 1, 3, 'How you doing?', ''),
(5, 1, 1, 'What are you doing?', ''),
(6, 1, 1, 'English', ''),
(7, 1, 1, 'English', 'Tamil');

-- --------------------------------------------------------

--
-- Table structure for table `score`
--

CREATE TABLE `score` (
  `progress_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `chapter_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `score` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `score`
--

INSERT INTO `score` (`progress_id`, `user_id`, `chapter_id`, `question_id`, `score`) VALUES
(1, 1, 2, 1, 2),
(2, 2, 2, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `contact` varchar(16) NOT NULL,
  `email` varchar(64) NOT NULL,
  `pasword` varchar(32) NOT NULL,
  `stats` int(5) NOT NULL DEFAULT 0,
  `dob` varchar(16) NOT NULL,
  `city` varchar(64) NOT NULL,
  `region_type` varchar(16) NOT NULL,
  `fathers_employment` varchar(64) NOT NULL,
  `mothers_employment` varchar(64) NOT NULL,
  `monthly_family_income` int(11) NOT NULL,
  `no_of_siblings` int(11) NOT NULL,
  `religion` varchar(32) NOT NULL,
  `community` varchar(32) NOT NULL,
  `educational_qualification` varchar(64) NOT NULL,
  `educational_institution` varchar(128) NOT NULL,
  `how_do_you_know` varchar(64) NOT NULL,
  `feedback` text DEFAULT NULL,
  `token` int(11) DEFAULT NULL,
  `pasword_token` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `name`, `contact`, `email`, `pasword`, `stats`, `dob`, `city`, `region_type`, `fathers_employment`, `mothers_employment`, `monthly_family_income`, `no_of_siblings`, `religion`, `community`, `educational_qualification`, `educational_institution`, `how_do_you_know`, `feedback`, `token`, `pasword_token`) VALUES
(1, 'demo', '1454567890', 'demodemo@demo.com', '5f4dcc3b5aa765d61d8327deb882cf99', 1, '12-12-1212', 'city', 'region_type', 'fathers_employment', 'mothers_employment', 1000, 5, 'religion', 'community', 'educational_qualification', 'educational_institution', 'how_do_you_know', NULL, NULL, 991288);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assessment_answers`
--
ALTER TABLE `assessment_answers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assessment_questions`
--
ALTER TABLE `assessment_questions`
  ADD PRIMARY KEY (`assessment_question_id`);

--
-- Indexes for table `assessment_score`
--
ALTER TABLE `assessment_score`
  ADD PRIMARY KEY (`assessment_id`);

--
-- Indexes for table `chapters`
--
ALTER TABLE `chapters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chapter_score`
--
ALTER TABLE `chapter_score`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`module_id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `score`
--
ALTER TABLE `score`
  ADD PRIMARY KEY (`progress_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`email`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answers`
--
ALTER TABLE `answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `assessment_answers`
--
ALTER TABLE `assessment_answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `assessment_questions`
--
ALTER TABLE `assessment_questions`
  MODIFY `assessment_question_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `assessment_score`
--
ALTER TABLE `assessment_score`
  MODIFY `assessment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `chapters`
--
ALTER TABLE `chapters`
  MODIFY `id` int(22) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `chapter_score`
--
ALTER TABLE `chapter_score`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(22) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `module_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `score`
--
ALTER TABLE `score`
  MODIFY `progress_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
